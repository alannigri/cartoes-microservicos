package br.com.ms.cartoes.cliente.repositories;

import br.com.ms.cartoes.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}

