package br.com.ms.cartoes.cliente.services;

import br.com.ms.cartoes.cliente.exceptions.ClienteNaoEncontradoException;
import br.com.ms.cartoes.cliente.models.Cliente;
import br.com.ms.cartoes.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente) {
        clienteRepository.save(cliente);
        return cliente;
    }

    public Cliente consultarCliente (int id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()){
            return cliente.get();
        }
        throw new ClienteNaoEncontradoException();
    }
}

