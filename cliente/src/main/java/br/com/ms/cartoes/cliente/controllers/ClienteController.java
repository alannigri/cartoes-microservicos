package br.com.ms.cartoes.cliente.controllers;

import br.com.ms.cartoes.cliente.models.Cliente;
import br.com.ms.cartoes.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente (@AuthenticationPrincipal Cliente cliente){
        return clienteService.criarCliente(cliente);
    }

    @GetMapping("/{id}")
    public Cliente consultarCliente (@PathVariable int id){
        return clienteService.consultarCliente(id);
    }
}

