package br.com.cartoes.fatura.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new CartaoClientDecoder();
    }

}
