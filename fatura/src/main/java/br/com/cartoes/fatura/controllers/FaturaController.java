package br.com.cartoes.fatura.controllers;

import br.com.cartoes.fatura.models.DTOs.Pagamento;
import br.com.cartoes.fatura.models.DTOs.RetornoAlteraStatusCartao;
import br.com.cartoes.fatura.models.Fatura;
import br.com.cartoes.fatura.services.FaturaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/fatura")

public class FaturaController {

    @Autowired
    private FaturaServices faturaServices;

//    @Autowired
//    private FaturaMapper mapper;

    @GetMapping("/{clienteId}/{cartaoId}")
    public Iterable<Pagamento> consultarFatura(@PathVariable int clienteId, @PathVariable int cartaoId) {
        Iterable<Pagamento> fatura = faturaServices.consultarFatura(clienteId, cartaoId);
        return fatura;
    }

    @PostMapping("/{clienteId}/{cartaoId}/pagar")
    @ResponseStatus(HttpStatus.OK)
    public Fatura pagarFatura(@PathVariable int clienteId, @PathVariable int cartaoId) {
        Fatura fatura = faturaServices.pagarFatura(clienteId, cartaoId);
        return fatura;
    }

    @PostMapping("/{clienteId}/{cartaoId}/expirar")
    @ResponseStatus(HttpStatus.OK)
    public RetornoAlteraStatusCartao bloqueiaCartao(@PathVariable int clienteId, @PathVariable int cartaoId) {
        RetornoAlteraStatusCartao retornoAlteraStatusCartao = new RetornoAlteraStatusCartao();
        String retorno = faturaServices.bloqueiaCartao(clienteId, cartaoId);
        retornoAlteraStatusCartao.setStatus(retorno);
        return retornoAlteraStatusCartao;
    }


}
