package br.com.cartoes.fatura.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartao não pertence ao cliente")
public class CartaoNaoPertenceAoCliente extends RuntimeException{
}
