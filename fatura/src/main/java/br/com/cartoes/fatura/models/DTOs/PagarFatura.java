package br.com.cartoes.fatura.models.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class PagarFatura {

    private int id;

    private double valorPago;

    @JsonProperty(value = "pagoEm")
    private LocalDate dataPagto;

}
