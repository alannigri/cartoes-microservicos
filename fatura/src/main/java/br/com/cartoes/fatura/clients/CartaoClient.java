package br.com.cartoes.fatura.clients;

import br.com.cartoes.fatura.models.DTOs.Cartao;
import br.com.cartoes.fatura.models.DTOs.EnvioAlteraStatusCartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("cartao/id/{id}")
    Cartao consultaCartaoPorId(@PathVariable int id);

    @PatchMapping("cartao/{numero}")
    Cartao alteraStatusCartao(@PathVariable String numero, @RequestBody EnvioAlteraStatusCartao ativo);
}