package br.com.cartoes.fatura.services;

import br.com.cartoes.fatura.clients.CartaoClient;
import br.com.cartoes.fatura.clients.ClienteClient;
import br.com.cartoes.fatura.clients.PagamentoClient;
import br.com.cartoes.fatura.exceptions.CartaoNaoPertenceAoCliente;
import br.com.cartoes.fatura.exceptions.FaturaVazia;
import br.com.cartoes.fatura.models.DTOs.Cartao;
import br.com.cartoes.fatura.models.DTOs.Cliente;
import br.com.cartoes.fatura.models.DTOs.EnvioAlteraStatusCartao;
import br.com.cartoes.fatura.models.DTOs.Pagamento;
import br.com.cartoes.fatura.models.Fatura;
import br.com.cartoes.fatura.repositories.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FaturaServices {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private PagamentoClient pagamentoClient;

    public List<Pagamento> consultarFatura(int clienteId, int cartaoId) {
        Cliente cliente = clienteClient.consultarCliente(clienteId);
        Cartao cartao = cartaoClient.consultaCartaoPorId(cartaoId);
        if (cartao.getId_Cliente() != cliente.getId()) {
            throw new CartaoNaoPertenceAoCliente();
        }
        return pagamentoClient.consultarPagamentos(cartaoId);
    }


    public Fatura pagarFatura(int clienteId, int cartaoId) {
//        Cliente cliente = clienteClient.consultarCliente(clienteId);
//        Cartao cartao = cartaoClient.consultaCartaoPorId(cartaoId);
//        if (cartao.getId_Cliente() != cliente.getId()) {
//            throw new CartaoNaoPertenceAoCliente();
//        }
//        List<Pagamento> pagamentos = pagamentoClient.consultarPagamentos(cartaoId);
        List<Pagamento> pagamentos = consultarFatura(clienteId, cartaoId);
        if (pagamentos.size() == 0) {
            throw new FaturaVazia();
        }
        double total = 0;
        for (Pagamento p : pagamentos) {
            total += p.getValor();
        }
        pagamentoClient.deletarPagamentos(cartaoId);

        Fatura fatura = new Fatura();
        fatura.setValorPago(total);
        fatura.setDataPagto(LocalDate.now());
        faturaRepository.save(fatura);
        return fatura;
    }

    public String bloqueiaCartao(int clienteId, int cartaoId) {
        Cartao cartao = cartaoClient.consultaCartaoPorId(cartaoId);
        Cliente cliente = clienteClient.consultarCliente(clienteId);
        if (cartao.getId_Cliente() != cliente.getId()) {
            throw new CartaoNaoPertenceAoCliente();
        }
        EnvioAlteraStatusCartao envioAlteraStatusCartao = new EnvioAlteraStatusCartao();
        envioAlteraStatusCartao.setAtivo(false);
        cartaoClient.alteraStatusCartao(cartao.getNumero(), envioAlteraStatusCartao);
        String status = "ok";
        return status;
    }
}
