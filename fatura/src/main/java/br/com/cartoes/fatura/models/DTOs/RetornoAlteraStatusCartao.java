package br.com.cartoes.fatura.models.DTOs;

public class RetornoAlteraStatusCartao {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
