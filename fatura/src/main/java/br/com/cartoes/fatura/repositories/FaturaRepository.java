package br.com.cartoes.fatura.repositories;

import br.com.cartoes.fatura.models.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {
}
