package br.com.cartoes.fatura.clients;

import br.com.cartoes.fatura.models.DTOs.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("cliente/{id}")
    Cliente consultarCliente(@PathVariable int id);

}
