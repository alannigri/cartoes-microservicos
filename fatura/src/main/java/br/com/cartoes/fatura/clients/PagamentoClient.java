package br.com.cartoes.fatura.clients;


import br.com.cartoes.fatura.models.DTOs.Pagamento;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@FeignClient(name = "pagamento", configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {

    @GetMapping("pagamentos/{id}")
    List<Pagamento> consultarPagamentos(@PathVariable int id);

    @DeleteMapping("pagamentos/{id}")
    boolean deletarPagamentos(@PathVariable int id);
}