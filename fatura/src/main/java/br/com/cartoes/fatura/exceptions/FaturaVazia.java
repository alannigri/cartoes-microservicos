package br.com.cartoes.fatura.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Fatura está zerada para pagamento")
public class FaturaVazia extends RuntimeException{
}
