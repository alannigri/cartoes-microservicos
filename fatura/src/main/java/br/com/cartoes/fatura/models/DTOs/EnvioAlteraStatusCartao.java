package br.com.cartoes.fatura.models.DTOs;

public class EnvioAlteraStatusCartao {

    private boolean ativo;

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
