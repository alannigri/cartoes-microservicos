package br.com.ms.cartoes.cartao.models.DTOs;

import br.com.ms.cartoes.cartao.models.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao criarCartao(EnvioCriarCartao envioCriarCartao) {
        Cartao cartao = new Cartao();
        cartao.setNumero(envioCriarCartao.getNumero());
        cartao.setId_Cliente(envioCriarCartao.getClienteId());
        return cartao;
    }

    public RespostaCartaoCriado cartaoCriado (Cartao cartao){
        RespostaCartaoCriado respostaCartaoCriado = new RespostaCartaoCriado();
        respostaCartaoCriado.setId(cartao.getId());
        respostaCartaoCriado.setNumero(cartao.getNumero());
        respostaCartaoCriado.setAtivo(cartao.isAtivo());
        respostaCartaoCriado.setClienteId(cartao.getId_Cliente());
        return respostaCartaoCriado;
    }

    public RespostaConsultaCartao consultaCartao (Cartao cartao){
        RespostaConsultaCartao respostaConsultaCartao = new RespostaConsultaCartao();
        respostaConsultaCartao.setId(cartao.getId());
        respostaConsultaCartao.setNumero(cartao.getNumero());
        respostaConsultaCartao.setClienteId(cartao.getId_Cliente());
        return respostaConsultaCartao;
    }

    public boolean alteraStatusCartao (EnvioAlteraStatusCartao e){
        boolean ativo = e.isAtivo();
        return ativo;
    }
}
