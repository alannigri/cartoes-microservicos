package br.com.ms.cartoes.cartao.client;

import br.com.ms.cartoes.cartao.exceptions.CartaoNaoEncontradoException;
import br.com.ms.cartoes.cartao.exceptions.ClienteNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new ClienteNaoEncontradoException();
        }else{
            return errorDecoder.decode(s, response);
        }

    }
}
