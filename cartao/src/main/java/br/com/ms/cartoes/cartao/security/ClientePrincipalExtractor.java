package br.com.ms.cartoes.cartao.security;

import br.com.ms.cartoes.cartao.models.DTOs.Cliente;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class ClientePrincipalExtractor implements PrincipalExtractor{

        @Override
        public Object extractPrincipal(Map<String, Object> map) {
            Cliente cliente= new Cliente();
            cliente.setName((String) map.get("name"));
            return cliente;
        }
    }