package br.com.ms.cartoes.cartao.models.DTOs;

public class RespostaConsultaCartao {

    private int id;
    private String numero;
    private int clienteId;

    public RespostaConsultaCartao() {
    }

    public RespostaConsultaCartao(int id, String numero, int clienteId) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

}

