package br.com.ms.cartoes.cartao.models.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EnvioCriarCartao {

    @NotNull
    @NotBlank
    private String numero;

    private int clienteId;

    public EnvioCriarCartao(String numero, int id) {
        this.numero = numero;
        this.clienteId = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}

