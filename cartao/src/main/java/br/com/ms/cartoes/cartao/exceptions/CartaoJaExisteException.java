package br.com.ms.cartoes.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão ja existe")
    public class CartaoJaExisteException extends RuntimeException{

}
