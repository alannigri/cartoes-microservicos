package br.com.ms.cartoes.cartao.client;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration extends OAuth2FeignConfiguration{

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteClientDecoder();
    }

}
