package br.com.ms.cartoes.cartao.repositories;

import br.com.ms.cartoes.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    public Optional<Cartao> findByNumero(String numero);
}
