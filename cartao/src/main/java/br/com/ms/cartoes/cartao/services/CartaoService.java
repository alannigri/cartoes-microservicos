package br.com.ms.cartoes.cartao.services;

import br.com.ms.cartoes.cartao.client.ClienteClient;
import br.com.ms.cartoes.cartao.exceptions.CartaoJaExisteException;
import br.com.ms.cartoes.cartao.exceptions.CartaoNaoEncontradoException;
import br.com.ms.cartoes.cartao.models.Cartao;
import br.com.ms.cartoes.cartao.models.DTOs.Cliente;
import br.com.ms.cartoes.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao criarCartao(Cartao cartao, Cliente clienteLogado) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(cartao.getNumero());
        if (cartaoOptional.isPresent()) {
            throw new CartaoJaExisteException();
        }
        Cliente cliente = clienteClient.consultarCliente(cartao.getId_Cliente()); //pra ver se o cliente existe
        if(!cliente.getName().equals(clienteLogado.getName())){
            throw new RuntimeException("Cartão não pertence ao cliente logado. Cliente logado: " + clienteLogado.getName());
        }
        cartao.setAtivo(false);
        cartaoRepository.save(cartao);
        return cartao;
    }

    public Cartao consultarCartaoPorNumero(String numero) {
        Optional<Cartao> cartao = cartaoRepository.findByNumero(numero);
        if (!cartao.isPresent()) {
            throw new CartaoNaoEncontradoException();
        }
        return cartao.get();
    }

    public Cartao consultarCartaoPorId(int id) {
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        if (!cartao.isPresent()) {
            throw new CartaoNaoEncontradoException();
        }
        return cartao.get();
    }

    public Cartao alteraStatusCartao(String numero, boolean ativo) {
        Optional<Cartao> cartao = cartaoRepository.findByNumero(numero);
        if (!cartao.isPresent()) {
            throw new CartaoNaoEncontradoException();
        }
        cartao.get().setAtivo(ativo);
        Cartao cartao1 = cartaoRepository.save(cartao.get());
        return cartao1;
    }
}

