package br.com.ms.cartoes.cartao.controllers;

import br.com.ms.cartoes.cartao.models.Cartao;
import br.com.ms.cartoes.cartao.models.DTOs.*;
import br.com.ms.cartoes.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCartaoCriado criarCartao(@RequestBody @Valid EnvioCriarCartao envioCriarCartao,
                                            @AuthenticationPrincipal Cliente cliente) {
        Cartao cartao = mapper.criarCartao(envioCriarCartao);
        cartaoService.criarCartao(cartao, cliente);
        return mapper.cartaoCriado(cartao);
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaCartaoCriado alteraStatusCartao(@PathVariable String numero, @RequestBody EnvioAlteraStatusCartao ativo) {
        boolean ativo1 = mapper.alteraStatusCartao(ativo);
        Cartao cartao = cartaoService.alteraStatusCartao(numero, ativo1);
        return mapper.cartaoCriado(cartao);
    }

    @GetMapping("/{numero}")
    public RespostaConsultaCartao consultaCartaoPorNumero(@PathVariable String numero) {
        Cartao cartao = cartaoService.consultarCartaoPorNumero(numero);
        return mapper.consultaCartao(cartao);
    }


    @GetMapping("/id/{id}")
    public Cartao consultaCartaoPorId(@PathVariable int id) {
        Cartao cartao = cartaoService.consultarCartaoPorId(id);
        return cartao;
    }
}