package br.com.ms.cartoes.pagamento.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de cartao se encontra offline")
public class CartaoClientException extends RuntimeException {
}
