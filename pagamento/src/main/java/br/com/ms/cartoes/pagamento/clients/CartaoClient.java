package br.com.ms.cartoes.pagamento.clients;

import br.com.ms.cartoes.pagamento.models.DTOs.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Cartao consultarCartaoPorId(@PathVariable int id);
}
