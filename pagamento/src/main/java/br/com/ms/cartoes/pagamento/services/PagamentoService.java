package br.com.ms.cartoes.pagamento.services;

import br.com.ms.cartoes.pagamento.clients.CartaoClient;
import br.com.ms.cartoes.pagamento.exceptions.CartaoNaoEstaAtivoException;
import br.com.ms.cartoes.pagamento.models.DTOs.Cartao;
import br.com.ms.cartoes.pagamento.models.Pagamento;
import br.com.ms.cartoes.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento realizarPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoClient.consultarCartaoPorId(pagamento.getIdCartao());
        if (!cartao.isAtivo()) {
            throw new CartaoNaoEstaAtivoException();
        }
        pagamentoRepository.save(pagamento);
        return pagamento;
    }

    public Iterable<Pagamento> consultarPagamentos(int idCartao) {
        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findAllByIdCartao(idCartao);
        return pagamentoIterable;
    }

    public boolean deletarPagamentos (int idCartao){
        pagamentoRepository.deleteIdCartao(idCartao);

        return true;
    }
}

