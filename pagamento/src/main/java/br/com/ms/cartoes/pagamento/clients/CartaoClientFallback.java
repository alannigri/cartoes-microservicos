package br.com.ms.cartoes.pagamento.clients;

import br.com.ms.cartoes.pagamento.models.DTOs.Cartao;

public class CartaoClientFallback implements CartaoClient {

    @Override
    public Cartao consultarCartaoPorId(int id) {
        throw new CartaoClientException();
    }
}
