package br.com.ms.cartoes.pagamento.models.DTOs;

import br.com.ms.cartoes.pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento entradaNovoPagamento (EnvioCriarNovoPagamento envioCriarNovoPagamento){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(envioCriarNovoPagamento.getDescricao());
        pagamento.setIdCartao(envioCriarNovoPagamento.getCartao_id());
        pagamento.setValor(envioCriarNovoPagamento.getValor());
        return pagamento;
    }

}
