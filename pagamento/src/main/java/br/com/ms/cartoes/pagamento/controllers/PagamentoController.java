package br.com.ms.cartoes.pagamento.controllers;


import br.com.ms.cartoes.pagamento.models.DTOs.EnvioCriarNovoPagamento;
import br.com.ms.cartoes.pagamento.models.DTOs.PagamentoMapper;
import br.com.ms.cartoes.pagamento.models.Pagamento;
import br.com.ms.cartoes.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper mapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento criarPagamento(@RequestBody EnvioCriarNovoPagamento entradaPagamentoDTO){
        Pagamento pagamento = mapper.entradaNovoPagamento(entradaPagamentoDTO);
        pagamento = pagamentoService.realizarPagamento(pagamento);
        return pagamento;
    }

    @GetMapping("/pagamentos/{idCartao}")
    public Iterable<Pagamento> consultarPagamentos(@PathVariable int idCartao){
        Iterable<Pagamento> pagamentoIterable = pagamentoService.consultarPagamentos(idCartao);
        return pagamentoIterable;
    }

    @DeleteMapping("pagamentos/{idCartao}")
     public boolean deletarPagamentos(@PathVariable int idCartao){
        boolean result = pagamentoService.deletarPagamentos(idCartao);
        return result;
    }
}
