package br.com.ms.cartoes.pagamento.repositories;

import br.com.ms.cartoes.pagamento.models.DTOs.Cartao;
import br.com.ms.cartoes.pagamento.models.Pagamento;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PagamentoRepository extends CrudRepository <Pagamento, Integer> {
    public Iterable<Pagamento> findAllByIdCartao(int idCartao);

    @Modifying
    @Transactional
    @Query(value = "delete from pagamento where id_cartao = :idCartao", nativeQuery = true)
    void deleteIdCartao(@Param("idCartao") int idCartao);

}
