package br.com.ms.cartoes.pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Cartão não está ativo")
public class CartaoNaoEstaAtivoException extends RuntimeException{
}